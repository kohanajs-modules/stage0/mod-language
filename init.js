const { KohanaJS } = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['language', require('./config/language')],
]));
