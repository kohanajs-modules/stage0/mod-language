module.exports = {
  route: '/:language(en|zh-hant|zh-hans)',
  default: 'en',
};
